#!/usr/bin/env python

import os
import sys
import time
import BaseHTTPServer
from SocketServer import ThreadingMixIn
import cgi
import Cookie
import mimetypes
import getpass
import json
import thread
import signal
import logging

from notes import NDB

LOG_LEVELS = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL
    }

def static_file(dirname, omit_dirname=True):
    l = []
    static_dir = os.walk(dirname)
    for root, dir, files in static_dir:
        for name in files:
            file_name = os.path.join(root, name)
            if omit_dirname:
                l.append(file_name[len(dirname):])
            else:
                l.append(file_name)
    return l


def config(pard={}):
    pard['ADDRESS'] = ''
    pard['DNS'] = 'localhost'
    pard['HTTP_PORT'] = 8767
    pard['APPSERVER_URI'] = '/'
    if pard['HTTP_PORT'] != 80:
        pard['APPSERVER'] = 'http://' + pard['DNS'] + ':' + str(pard['HTTP_PORT']) + pard['APPSERVER_URI']
    else:
        pard['APPSERVER'] = 'http://' + pard['DNS'] + pard['APPSERVER_URI']
    pard['TITLE'] = 'NoteDown (BETA!)'
    pard['STATIC_FILE'] = []
    pard['STATIC_DIR'] = 'static/'
    pard['AUTOSAVE'] = 300
    pard['LOGLEVEL'] = 'debug'
    pard['HOME'] = os.environ['HOME']
    return pard


def main(pard, render=None):
    pard['action'] = pard.get('action', 'start')
#     pard['page_header'] = pard['TITLE']
#     pard['css'] = css(pard)
#     pard['javascript'] = javascript(pard)
#     pard['sidebar'] = ''
#     pard['main_body'] = ''
#     pard['msg'] = ''
    pard['user'] = pard.get('user', getpass.getuser())
    pard['order_by'] = pard['HTTP_COOKIE'].get('order_by', 'key')
    
    # --------------------------- start ----------------------------
    if pard['action'] == 'start':
        note_list = NDB.get_list(pard['user'], pard['order_by'])
        pard['key_list'] = render_key_list(pard, note_list)
        
        pard.setdefault('order_by', 'key')
        if pard['order_by'] == 'upd_ts':
            pard['sort_by_date_selected'] = 'selected'
            pard['sort_by_date_value'] = 'Y'
        else:
            pard['sort_by_date_selected'] = ''
            pard['sort_by_date_value'] = 'N'

        if render:
            pard['html'] = render.base(pard)
        else:
            html = open('base.html', 'r').read()
            pard['html'] = html % pard
        return pard
    
    # --------------------------- preview ----------------------------
    elif pard['action'] == 'preview':
        pard['title'] = pard['note_key'].replace(' ', '_').replace('/', '').replace('-', '')
        if render:
            pard['html'] = render.preview(pard)
        else:
            html = open('preview.html', 'r').read()
            pard['html'] = html % pard
        return pard
    
    # ----------------------- get_key_list ------------------------
    elif pard['action'] == 'get_key_list':
        note_list = NDB.search(pard['user'], pard['filter'], pard['order_by'])
        pard['html'] = render_key_list(pard, note_list)
        return pard
    
    # ----------------------- get_note ------------------------
    elif pard['action'] == 'get_note':
        note = NDB.get_note(pard['user'], pard['key'])
        note['escaped_content'] = cgi.escape(note['content'])
        pard['html'] = json.dumps(note)
        return pard

    # ----------------------- edit_note ------------------------
    elif pard['action'] == 'edit_note':
        note = NDB.get_note(pard['user'], pard['key'])
        pard['html'] = render_edit_note(pard, note)
        return pard
    
    # ----------------------- save_note ------------------------
    elif pard['action'] == 'save_note':
        NDB.set_note(pard['user'], pard['key'], pard['value'])
        pard['html'] = '<strong>Saved</strong>: %s' % cgi.escape(pard['key'])
        return pard
    
    # ----------------------- new_note ------------------------
    elif pard['action'] == 'new_note':
        note = NDB.get_note(pard['user'], pard['key'])
        if not note and pard['key'].strip():
            note = NDB.set_note(pard['user'], pard['key'])
        pard['html'] = json.dumps(note)
        return pard
    
    # ----------------------- delete_note ------------------------
    elif pard['action'] == 'delete_note':
        NDB.delete(pard['user'], pard['key'])
        pard['html'] = '<strong>Deleted</strong>: %s' % cgi.escape(pard['key'])
        return pard
    
    # ----------------------- test ------------------------
    elif pard['action'] == 'test':
        #tokens = json.loads(pard['tokens'])
        print type(pard['mdhtml'])
        open('data/pippo.html', 'wb').write(pard['mdhtml'])
        pard['html'] = ''
        return pard
    
    else:
        pard['html'] = 'Function not available. ("%(action)s")' % pard
    
    return pard


def render_key_list(pard, note_list):
    h = []
    h.append('<ul class="nav nav-tabs nav-stacked">')
    for el in note_list:
        el['mtime'] = smart_upd_ts(el['upd_ts'])
        h.append('<li><a href="#" data-key="%(key)s"> <i class="icon-chevron-right"></i> <span class="ts">%(mtime)s</span> %(key)s</a></li>' % el)
    h.append('</ul>')
    return '\n'.join(h)


def render_edit_note(pard, note):
    if not note:
        return ''
    rows = len(note['content'].split('\n'))
    note['rows'] = rows + 3
    note['escaped_content'] = cgi.escape(note['content'])
    html = """
    <textarea
        class="input-block-level"
        id="note_textarea"
        rows="%(rows)s"
        data-key="%(key)s"
        >%(escaped_content)s</textarea>
    <span id="ajaxFired" class="wrap"></span>
    """ % note
    return html


# -------------------------------------------------------------------------- #
# Utility
# -------------------------------------------------------------------------- #
import binascii
import uuid

def pair_string(s):
    if s.startswith('/'):
        s = s[1:]
    ll = s.split('/')
    d = {}
    for i in range(0,len(ll),2):
        if ll[i]:
            if i+1 < len(ll):
                d[ll[i]] = ll[i+1]
            else:
                d[ll[i]] = ''
    return d


def check_dir(dir_name):
    """Controlla se esiste la directory"""
    if os.path.exists(dir_name):
        if os.path.isdir(dir_name):
            pass
        else:
            os.remove(dir_name)
            os.mkdir(dir_name)
    else:
        os.mkdir(dir_name)

def get_token():
    return binascii.b2a_hex(uuid.uuid4().bytes)


def multivalue(pard, key):
    l = pard.get(key, [])
    if not l:
        l = []
    elif isinstance(l, str):
        l = [l]
    return l


def smart_upd_ts(ts):
    now = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    if ts[:10] == now[:10]:
        return ts[11:16]
    else:
        t = time.strptime(ts[0:10], '%Y-%m-%d')
        if ts[:4] == now[:4]:
            tt = time.strftime('%d %b', t)
        else:
            tt = time.strftime('%d %b %Y', t)
        return tt

#####################################################################
class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
    
    def do_GET(self):
        """Respond to a GET request."""
        self.send_response(200)
        self.driver()
        
    def do_POST(self):
        """Respond to a POST request."""
        self.send_response(200)             
        self.driver()
        
    def load_env(self):
        env = {}
        env['SERVER_SOFTWARE'] = self.version_string()
        env['SERVER_NAME'] = self.server.server_name
        env['GATEWAY_INTERFACE'] = 'CGI/1.1'
        env['SERVER_PROTOCOL'] = self.protocol_version
        env['SERVER_PORT'] = str(self.server.server_port)
        env['REQUEST_METHOD'] = self.command
        #upath = urllib.unquote(self.path)
        upath = self.path
        env['PATH_INFO'] = upath
        i = upath.rfind('?')
        if i > 0:
            rest, query = upath[:i], upath[i+1:]
        else:
            query = ''
            rest = upath
        i = rest.find('/')
        if i > -1:
            dummy, rest = rest[:i], rest[i+1:]
        else:
            rest = ''
        env['REST_STRING'] = rest
        if query:
            env['QUERY_STRING'] = query
        host = self.address_string()
        if host != self.client_address[0]:
            env['REMOTE_HOST'] = host
        env['REMOTE_ADDR'] = self.client_address[0]
        if self.headers.typeheader is None:
            env['CONTENT_TYPE'] = self.headers.type
        else:
            env['CONTENT_TYPE'] = self.headers.typeheader
        length = self.headers.getheader('content-length')
        if length:
            env['CONTENT_LENGTH'] = length
        accept = []
        for line in self.headers.getallmatchingheaders('accept'):
            if line[:1] in "\t\n\r ":
                accept.append(line.strip())
            else:
                accept = accept + line[7:].split(',')
        env['HTTP_ACCEPT'] = ','.join(accept)
        ua = self.headers.getheader('user-agent')
        if ua:
            env['HTTP_USER_AGENT'] = ua
        co = filter(None, self.headers.getheaders('cookie'))
        if co:
            co = ', '.join(co)
            env['HTTP_COOKIE'] = self.get_cookie(co)
        else:
            env['HTTP_COOKIE'] = {}
        x_req = self.headers.getheader('X-Requested-With')
        if x_req:
            env['X_REQUESTED_WITH'] = x_req # XMLHttpRequest = Ajax
        # XXX Other HTTP_* headers
        # Since we're setting the env in the parent, provide empty
        # values to override previously set values
        for k in ('QUERY_STRING', 'REMOTE_HOST', 'CONTENT_LENGTH',
                  'HTTP_USER_AGENT', 'HTTP_COOKIE', 'X_REQUESTED_WITH'):
            env.setdefault(k, "")
        return env
    
    
    def get_cookie(self, http_cookie=''):
        d = {}
        if http_cookie:
            c = Cookie.SimpleCookie(http_cookie)
            for m in c:
                key = c[m].key
                value = c[m].value
                d[key] = value
        return d
    
    
    def driver(self, req=''):
        start = time.time()
        #sys.stderr = sys.stdout
        pard = {}
        pard.update(self.load_env())
        form = cgi.FieldStorage(fp=self.headers.fp, headers=None,
                    outerboundary="", environ=pard,
                    keep_blank_values=1, strict_parsing=0)
        for k in form.keys():
            if k not in pard: # disable environment key overwrite
                if (k[0:12] == 'file_upload_') and (form[k].filename != ''):
                    pard['file_upload'].append(
                        {'filename': form[k].filename,
                         'content': form.getvalue(k, ''),
                         'form_id': k})
                else:
                    pard[k] = form.getvalue(k, '')
        # Converts directory string (the path before query string)
        # in key, value parameters
        # example: http://appserver/pippo/pluto ==> pard['pippo'] = 'pluto'
        if pard['REST_STRING']:
            pard.update(pair_string(pard['REST_STRING']))
        pard = config(pard)
        if  pard['REST_STRING'] \
        and pard['REST_STRING'] in STATIC_FILE:
            fn = os.path.join(pard['STATIC_DIR'], pard['REST_STRING'])
            buf = open(fn, 'rb').read()
            ctype, encoding = mimetypes.guess_type(fn)
            self.send_header("Content-type", ctype)
            self.send_header("Content-Length", str(len(buf)))
            self.send_header("Cache-Control", 'max-age=86400') ######################## Per Sviluppo commentare
            self.end_headers()
            self.wfile.write(buf)
        else:
            pard = main(pard)
            self.send_header("Content-type", "text/html")
            self.send_header("Content-Length", str(len(pard['html'])))
            self.end_headers()
            if isinstance(pard['html'], str):
                buf = pard['html'].decode('utf8')
            else:
                buf = pard['html']
            buf = buf.encode('utf8', 'xmlcharrefreplace')
            self.wfile.write(buf)
            stop = time.time()
            #self.wfile.write(repr(stop-start)[0:5])
        self.wfile.close()


#####################################################################
class ThreadingServer(ThreadingMixIn, BaseHTTPServer.HTTPServer):
    pass


def start(pard):
    
    ## Logging configuration
    level = LOG_LEVELS.get(pard['LOGLEVEL'], logging.NOTSET)
    logging.basicConfig(level=level)
    
    ## instantiate Notes class
    NDB.open(autostore=pard['AUTOSAVE'])
    
    ## Configure httpd server and RequestHandler
    serveraddr = (pard['ADDRESS'], pard['HTTP_PORT'])
    global STATIC_FILE
    STATIC_FILE = static_file(pard['STATIC_DIR'])
    httpd = ThreadingServer(serveraddr, RequestHandler)
    print time.asctime(), "Server Starts - %s:%s" % serveraddr
    
    ## Open webbrowser
    if '-o' in sys.argv:
        import webbrowser
        webbrowser.open_new_tab(pard['APPSERVER'])
    
    ## Configure clean exit
    def do_stop(signum=None, stackframe=None):
        NDB.close()
        httpd.server_close()
        print time.asctime(), "Server Stops - %s:%s" % serveraddr
        raise SystemExit
    
    ## Configure signal handler
    signal.signal(signal.SIGTERM, do_stop)
    signal.signal(signal.SIGINT, do_stop)
    
    ## Start Server
    httpd.serve_forever()


if __name__ == '__main__':
    pard = config()
    start(pard)
