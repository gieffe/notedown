#!/usr/bin/env python

import subprocess
import logging
import os
import sys

FORMAT = '%(levelname)-8s %(asctime)-15s %(name)-2s %(message)s'
logging.basicConfig(format=FORMAT,filename='./gaython.log',level=logging.DEBUG)
logger = logging.getLogger('gaython')

def singleton(cls):
    instances = {}
    def getinstance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return getinstance


@singleton
class Git(object):
    def __init__(self, gitdir):
        try:
            os.makedirs(gitdir)
        except OSError:
            pass
        self.gitdir = gitdir
        self.logformat = {
            '%H':   '',
            '%h':   '',
            '%T':   '',
            '%t':   '',
            '%P':   '',
            '%p':   '',
            '%an':  '',
            '%ae':  '',
            '%a':   '',
            '%ar':  '',
            '%cn':  '',
            '%ce':  '',
            '%cd':  '',
            '%cr':  '',
            '%s':   '',
        }

    class GitError(Exception):
        def __init__(self, message):
            Exception.__init__(self, message)

    def runCommand(self, command):
        command.insert(0, 'git')
        myenv = os.environ
        myenv['GIT_DIR'] = self.gitdir + '/.git'
        myenv['GIT_WORK_TREE'] = self.gitdir
        logger.debug(command)
        cmd = subprocess.Popen(command, stdout = subprocess.PIPE , stderr = subprocess.PIPE, env = myenv)
        out, err = cmd.communicate()
        returncode = cmd.returncode
        logger.debug("Output %s " % out)
        logger.debug("Ret %s " % returncode)
        if err:
            logger.debug("Error %s" % err)
        if returncode != 0 and command[1] != 'commit':
            raise self.GitError("Returncode: %s\nstdout: %s\nsterr:%s" % (returncode, out, err))
        return { 'out': out, 'err': err, 'ret': returncode }


    def rm(self, filename):
        self.logger = logging.getLogger('git rm')
        self.runCommand([ sys._getframe().f_code.co_name.lower() ] + [ filename ])

    def mv(self,  filename, newname):
        self.logger = logging.getLogger('git mv')
        self.runCommand([ sys._getframe().f_code.co_name.lower()] + [ filename , newname] )

    def init(self):
        self.logger = logging.getLogger('git init')
        try:
            self.runCommand([ 'status' ])
        except self.GitError:
            self.runCommand([ sys._getframe().f_code.co_name.lower() ])

    def add(self, filename=None, filelist=None):
        self.logger = logging.getLogger('git add')
        param_add_list = []
        if filename:
            param_add_list = [ filename ]
        elif filelist:
            param_add_list = filelist
        else:
            param_add_list = [ '.' ]
        self.runCommand([ sys._getframe().f_code.co_name.lower()] + param_add_list )

    def commit(self, message=None, filename=None, filelist=None):
        self.logger = logging.getLogger('git commit')
        param_add_list = []
        if filename:
            param_add_list = [ filename, '-m', message if message else "Committed %s" % filename ]
        elif filelist:
            param_add_list = filelist + [ '-m', message if message else "Committed %s" % str(filelist) ]
        else:
            param_add_list = ['-a', '-m', message if message else "Committed everything"]
        self.runCommand([ sys._getframe().f_code.co_name.lower()] + param_add_list )

    def log(self):
        self.logger = logging.getLogger('git list commit')
        cmdret = self.runCommand([ 'log', '--pretty=format:%s' % "##".join(sorted(self.logformat.keys())) ])
        commitlist = []
        for line in cmdret['out'].split('\n'):
            commitdict = {}
            counter = int(0)
            for word in line.split('##'):
                commitdict[sorted(self.logformat.keys())[counter]] = word
                counter += 1
            commitlist.append(commitdict)
        return commitlist

    def checkout(self, refs, filename=None):
        self.logger = logging.getLogger('git checkout refs')
        param_add_list = []
        if filename:
            param_add_list = [ refs , "--", filename ]
        else:
            param_add_list = [ refs ]
        self.runCommand([ sys._getframe().f_code.co_name.lower()] + param_add_list )


if __name__ == "__main__":
    gitdir = sys.argv[1]
    git = Git(gitdir)
    git.init()
    FILE1 = "foo"
    FILE2 = "bar"
    FILE3 = "baz"
    os.system('touch %s/%s' % (gitdir , FILE1))
    os.system('touch %s/%s' % (gitdir , FILE2))
    git.add(filename=FILE1)
    git.commit(filename=FILE1)
    git.add(filelist=[FILE2])
    git.commit(filelist=[FILE2])
    git.rm(FILE2)
    git.mv(FILE1, FILE3)
    os.system('echo %s > %s/%s' % ("some code" , gitdir, FILE3))
    git.add(filelist=[FILE3])
    git.commit(message="automated test commit")
    git.log()
