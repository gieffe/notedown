NoteDown
=========
Notedown is a simple note taker with [Markdown] support.

### Usage ###
Clone repository:

    git clone https://gieffe@bitbucket.org/gieffe/notedown.git

Run:

    cd notedown
    ./notedown.py -o

-o option open http://localhost:8768 in a new window of your favorite browser

`NoteDown is still in Beta Version, use at your own risk.`

### Note ###
* Notedown saves your note in json formatted file. You can find it here:  
  `notedown/data/yourusername.json`
* Notedown supports [GitHub Flavored Markdown]

### On the Shoulders of Giants ###
Notedown uses these open source projects to work:

* [marked.js] markdown parser and compiler written in javascript
* [Twitter Bootstrap] - Intuitive, and powerful front-end framework
* [jQuery] - Do you know?

### License ###
MIT

[Markdown]: http://daringfireball.net/projects/markdown/
[marked.js]: https://github.com/chjj/marked
[GitHub Flavored Markdown]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[Twitter Bootstrap]: http://twitter.github.com/bootstrap/
[jQuery]: http://jquery.com
[GFM Spec]: https://help.github.com/articles/github-flavored-markdown

-----------------------------------------------------------
