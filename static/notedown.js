/*
 NoteDown Javascript
-------------------------------------------------------------------------- */

// -------------------------------------------------------------------------
// global variables
var url = $("#appserver").val();
var current_note = {};

//var converter = Markdown.getSanitizingConverter(); //Markdown.Converter.js

marked.setOptions({
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  langPrefix: 'lang-',
  highlight: null
});

$.cookie.defaults.expires = 7;

function empty_note(note) {
    return (note === undefined || jQuery.isEmptyObject(note));
}

// -------------------------------------------------------------------------
// Highlight active note
function set_active_note(note_key) {
    if (note_key != undefined || note_key === '') {
        var active_link = jQuery('#key_list li.active');
        var new_active_link = $('[data-key="' + note_key + '"]');
        active_link.removeClass('active');
        new_active_link.closest('li').addClass('active');
        $('#action_bar').show();
    }
    else {
        $('#action_bar').hide();
    }
}


// -------------------------------------------------------------------------
// Render Note Textarea giving "note" json object and "ele"ment container
function render_note_textarea(note, ele) {
    if (empty_note(note)) {
        note = {
            key: '',
            content: ''
        }
    }
    ele.html('<textarea id="note_textarea" class="input-block-level"></textarea>');
    var tarea = $('#note_textarea');
    tarea.data('key', note.key);
    tarea.val(note.content);
    resize_textarea('#note_textarea');
}

// -------------------------------------------------------------------------
// Render Markdown to html preview giving "note" json object
function preview_note(note) {
    if (empty_note(note)) {
        note = {
            key: '',
            content: ''
        }
    }
    var tmp = note.content;
    tmp = tmp.replace(/\n/g, '  \n')
    md = marked(tmp);
    return md;
}


// -------------------------------------------------------------------------
// set edit mode
function set_edit_mode() {
    $(view_me).removeClass('disabled');
    $(view_me).removeAttr('disabled');
    $(edit_me).addClass('disabled');
    $(edit_me).attr('disabled', 'disabled');
    $('#btn_preview').show();
    render_note_textarea(current_note, $('#note_area_left'));
    $('#note_textarea').focus();
}

// -------------------------------------------------------------------------
// set view mode
function set_view_mode() {
    $(edit_me).removeClass('disabled');
    $(edit_me).removeAttr('disabled');
    $(view_me).addClass('disabled');
    $(view_me).attr('disabled', 'disabled');
    $('#btn_preview').hide();
    $('#note_area_left').html(preview_note(current_note));
    hide_preview();
}


// -------------------------------------------------------------------------
// Get note
var get_note = function (e) {
    var el = jQuery(this);
    var key = el.data('key');
    var note = ajax_get_note(key);
    current_note = note;
    if (note && !jQuery.isEmptyObject(note)) {
        set_view_mode();
        $('#ajaxFired').html('');
        set_active_note(key);
    };
    return false;
}

// -------------------------------------------------------------------------
// Get note list
var timeout2;
var get_key_list = function(e) {
    clearTimeout(timeout2);
    clear_note_area();
    var self = e.target;
    timeout2 = setTimeout(function () {
        var filter = jQuery(self).val();
        var resp = ajax_get_note_list(filter);
        jQuery('#key_list').html(resp);
    }, 250);
}
// -------------------------------------------------------------------------
// Delete note
var delete_note = function() {
    if (!empty_note(current_note)) {
        var note_key = current_note.key;
        if (confirm('Do you want to delete this note?')) {
            var result = ajax_delete_note(note_key);
            clear_note_area();
            $('#ajaxFired').html(result);
            $('#filter-box').trigger('keyup');
        }
    }
}

// -------------------------------------------------------------------------
// New note
var new_note = function(e){
    note_key = $('#filter-box').val();
    // "Get or Set" note via ajax
    note = ajax_new_note(note_key);
    current_note = note;
    if (note && !jQuery.isEmptyObject(note)) {
        // clear result area "ajaxFired"
        $('#ajaxFired').html('');
        // refresh sidebar with note list and highlight active note
        var resp = ajax_get_note_list(note_key);
        jQuery('#key_list').html(resp);
        set_active_note(note_key);
        // Set action bar to edit mode
        set_edit_mode();
    }
    else {
        clear_note_area();
    };
    e.preventDefault();// prevent the default anchor functionality
}

// -------------------------------------------------------------------------
// Event Listener
// -------------------------------------------------------------------------
// Click on key list element (link)
jQuery(document).on('click', '#key_list a', get_note);

// Input filter-box (Filter note list)
$('#filter-box').bind({
    keyup: function(e) {
        if (e.which == 13) {
            new_note(e);
            //e.stopPropagation();
        }
        else {
            get_key_list(e);
        }
    },
    keydown: function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    }
});

// Clear filter-box
var clear_me = '[data-click="clear"]';
$(clear_me).click(function(e){
    var el = $(this);
    var target = el.data('target');
    $(target).val('');
    clear_note_area();
    $(target).trigger('keyup');
    e.preventDefault();
    //$('[data-toggle="dropdown"]').parent().removeClass('open'); //close dropdown
    //return false;
});

// Add note tooltip
$('#btn_add_note').tooltip({
    placement: 'bottom'
});

// Save Textarea on "input" event
// Todo: scrivere ajax_save_note ...
var timeout;
$(document).on('input', '#note_textarea', function () {
  clearTimeout(timeout);
  $('#ajaxFired').html('<strong>Typing...</strong>');
    var self = this;
    current_note.content = $(self).val()
    resize_textarea(self);
    $('#note_area_right').html(preview_note(current_note));
    timeout = setTimeout(function () {
        parj = {}
        var el = $(self);
        parj['key'] = el.data('key');
        parj['action'] = 'save_note';
        parj['value'] = el.val();
        jQuery.post(url, parj, function(data){
            jQuery('#ajaxFired').html(data);
        });
    }, 1000);
});

// Textare Grab Tab
$(document).on('keydown', '#note_textarea', function(e) {
  var keyCode = e.keyCode || e.which;

  if (keyCode == 9) {
    e.preventDefault();
    var start = $(this).get(0).selectionStart;
    var end = $(this).get(0).selectionEnd;

    // set textarea value to: text before caret + tab + text after caret
    $(this).val($(this).val().substring(0, start)
                + "\t"
                + $(this).val().substring(end));

    // put caret at right position again
    $(this).get(0).selectionStart =
    $(this).get(0).selectionEnd = start + 1;
    $(this).trigger('input')
  }
});


// -------------------------------------------------------------------------
// Action Bar
// -------------------------------------------------------------------------
var view_me = '[data-click="view"]';
var edit_me = '[data-click="edit"]';
var delete_me = '[data-click="delete"]';
var print_me = '[data-click="print"]';
var test_me = '[data-click="test"]';

var add_note = '[data-click="add-note"]';
//var note_areadata_dblclick = '[data-dblclick="edit"]';
var btn_toggle_sidebar = '[data-click="toggle-sidebar"]';
var btn_preview = '#btn_preview';


// View Mode
$(view_me).click(set_view_mode);

// Edit Mode
$(edit_me).click(set_edit_mode);

// Delete Note 
$(delete_me).click(delete_note);

// Print Note
$(print_me).click(function (){
    if (!empty_note(current_note)) {
        var note_key = current_note.key;
        var url = $("#appserver").val();
        var data = {
            action: 'preview',
            note_key: note_key,
        }
        var qs = $.param(data);
        window.open(url+'?'+qs);
    }
});

// Toggle Sort by Date
$('[data-click="sort-by-date"]').click(function (e){
    var el = $('#sort-by-date');
    var value = el.data('value');
    if (value == 'N') {
        el.addClass('selected');
        el.data('value', 'Y');
        $.cookie('order_by', 'upd_ts');
    }
    else {
        el.removeClass('selected');
        el.data('value', 'N');
        $.cookie('order_by', 'key');
    }
    $('#filter-box').trigger('keyup'); // for key list refresh!
    e.preventDefault(); // prevent the default anchor functionality
});


// Test
$(test_me).click(function (){
    if (!empty_note(current_note)) {
        var tokens = marked.lexer(current_note.content);
        var html = marked(current_note.content)
        var parj = {}
        parj.tokens = JSON.stringify(tokens);
        parj.action = 'test';
        parj.mdhtml = html;
        jQuery.post(url, parj);
    }
});

// Add new note
$(add_note).click(new_note);

// Edit on dbclick on Note area
//$(note_areadata_dblclick).dblclick(set_edit_mode);

// Toggle Sidebar
$(btn_toggle_sidebar).click(function (){
    if ($(btn_toggle_sidebar).data('action') === 'hide') {
        $(btn_toggle_sidebar).data('action', 'show');
        $(btn_toggle_sidebar).html('Show Sidebar');
        $('#div_main').toggleClass('span9 span12');
        $('#div_main').toggleClass('no-sidebar');
        $('#div_sidebar').hide();
    }
    else {
        $(btn_toggle_sidebar).data('action', 'hide');
        $(btn_toggle_sidebar).html('Hide Sidebar');
        $('#div_main').toggleClass('span9 span12');
        $('#div_main').toggleClass('no-sidebar');
        $('#div_sidebar').show();
    }
});

//Side by side preview
$(btn_preview).tooltip({placement: 'bottom'});
$(btn_preview).click(function (){
    var status = $(btn_preview).data('status');
    if (status === 'hide') {
        show_preview();
    }
    else {
        hide_preview();
    }
});


// Show right-side preview
function show_preview() {
    var status = $(btn_preview).data('status');
    if (status === 'hide') {
        $('#note_area_right').addClass('span6');
        $('#note_area_left').removeClass('span12');
        $('#note_area_left').addClass('span6');
        $('#note_area_right').show();
        $(btn_preview).text('Hide Preview');
        $('#note_area_right').html(preview_note(current_note));
        $(btn_preview).data('status', 'show');
    }
}

// Hide right-side preview
function hide_preview() {
    var status = $(btn_preview).data('status');
    if (status === 'show') {
        $('#note_area_right').removeClass('span6');
        $('#note_area_left').removeClass('span6');
        $('#note_area_left').addClass('span12');
        $('#note_area_right').hide();
        $(btn_preview).text('Preview');
        $(btn_preview).data('status', 'hide');
    }
}



// --------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// Clear Note Area
function clear_note_area() {
    $('#note_area_left').html('');
    $('#note_area_right').hide();
    reset_action_bar();
    current_note = {}
}

// ---------------------------------------------------------------------------
// Reset action bar
function reset_action_bar() {
    //$(edit_me).removeClass('disabled');
    //$(view_me).addClass('disabled');
    set_view_mode();
    $('#action_bar').hide();
}

// ---------------------------------------------------------------------------
// Resize textarea
function resize_textarea (tarea) {
    var buf = $(tarea).val();
    var ll = buf.split('\n');
    var rows = ll.length + 3;
    var tarea_rows = $(tarea).attr('rows');
    if (rows != tarea_rows) {
        $(tarea).attr('rows', rows);
    }
}


// ---------------------------------------------------------------------------
// request note data via ajax
function ajax_get_note( note_key ) {
    var response = null;
    $.ajax({
        url: $("#appserver").val(),
        async: false,
        data: {
            action: 'get_note',
            key: note_key,
        },
        success: function(data) {
            response = jQuery.parseJSON(data);
        },
        error: function() {
            alert("D'oh! Server unreachable.");
            response = {};
        }
    });

    return response;
}

// ---------------------------------------------------------------------------
// delete note via ajax
function ajax_delete_note( note_key ) {
    var response = null;
    $.ajax({
        url: $("#appserver").val(),
        async: false,
        data: {
            action: 'delete_note',
            key: note_key,
        },
        success: function(data) {
            response = data;
        },
        error: function() {
            alert("D'oh! Server unreachable.");
            response = '';
        }
    });

    return response;
}

// ---------------------------------------------------------------------------
// create new note via ajax
function ajax_new_note ( note_key ) {
    var response = null;
    $.ajax({
        url: $("#appserver").val(),
        async: false,
        data: {
            action: 'new_note',
            key: note_key,
        },
        success: function(data) {
            response = jQuery.parseJSON(data);
        },
        error: function() {
            alert("D'oh! Server unreachable.");
            response = {};
        }
    });

    return response;
}

// ---------------------------------------------------------------------------
// get list of note via ajax
function ajax_get_note_list ( filter ) {
    var response = null;
    if (!filter) {
        var filter = ''
    }
    $.ajax({
        url: $("#appserver").val(),
        async: false,
        data: {
            action: 'get_key_list',
            filter: filter
        },
        success: function(data) {
            response = data;
        },
        error: function() {
            alert("D'oh! Server unreachable.");
            response = '';
        }
    });

    return response;
}


// ---------------------------------------------------------------------------
// escape_tag
function escape_tag(str) {
   return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

// ---------------------------------------------------------------------------
// Print
function print_note() {
    var main_container = $('#main_container');
    var note_key = main_container.data('key');
    var note = ajax_get_note(note_key);
    if (note && !jQuery.isEmptyObject(note)) {
        var buf = '#' + note.key + '\n\n';
        buf = buf + '_Last updated ' + note.upd_ts + ' by ' + note.user + '_\n';
        buf = buf + '____\n\n' + note.content;
        md = marked(buf);
        main_container.html(md);
        window.print();
    };
}
