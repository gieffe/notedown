### ToDo ###
*(e possibili sviluppi dell'applicazione)*

- Bug se creo una chiave che contiene `"`
- Backup o meglio rotate dello storage su json.
- gestione della history e degli undo
- Backup and Restore (su Dropbox?)
- Print (PDF?)
- Ordinamento delle chiavi delle note per data modifica
  + mostrare data (ora) ultima modifica
- Completare README.md del progetto
- json su Dropbox?
- Possibili Fork:
  1. versione stand-alone praticamente mono utente: me la installo sulla mia macchina e la uso in locale
  2. versione web-app (cambio dello storage e gestione autenticazione)  
      sharing delle note con altri utenti.  


### Done
- Thread o processo di store dei dati sempre attivo. Adesso (prima - ndr) e' in memoria salva solo quando esce. 
