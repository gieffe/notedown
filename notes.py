import time
import json
import os
import logging
import hashlib

# -------------------------------------------------------------------------- #
# Utility
# -------------------------------------------------------------------------- #
def check_dir(dir_name):
    """Controlla se esiste la directory"""
    if os.path.exists(dir_name):
        if os.path.isdir(dir_name):
            pass
        else:
            os.remove(dir_name)
            os.mkdir(dir_name)
    else:
        os.mkdir(dir_name)


def sort_by(l, n, key=None):
    # For case insensitive sort for string use:
    # key = ci_sort = lambda (key, x): key.lower()
    nlist = map(lambda x, n=n: (x[n], x), l)
    nlist.sort(key=key)
    return map(lambda (key, x): x, nlist)


# -------------------------------------------------------------------------- #
# AutoStore
# -------------------------------------------------------------------------- #
import threading

class AutoStore(threading.Thread):
    """Class to store data in background every @dalay seconds
    
    Example:
        
        t = AutoStore(save_function, delay=60)
        t.start()
        ... do something ...
        t.join() # call thread join before exit.
    
    Author:
        gianfranco (gmessori@gmail.com)
    
    """
    
    def __init__(self, function, args, delay=60, name='AutoStore'):
        super(AutoStore, self).__init__(name=name)
        self._stop = threading.Event()
        self.delay = delay
        self.counter = 0
        self.function = function
        self._args = args
    
    def stop(self):
        if not self._stop.isSet():
            logging.info('%s stopped!' % self.getName())
            self._stop.set()
    
    def run(self):
        """ main control loop """
        while not self._stop.isSet():
            self.counter += 1
            logging.info('Save %s' % self.counter)
            self.function(*self._args)
            self._stop.wait(self.delay)
    
    def join(self, timeout=None):
        """ Stop the thread and wait for it to end """
        self.stop()
        super(AutoStore, self).join(timeout)
    


# -------------------------------------------------------------------------- #

class Notes(dict):
    """
    ToDo
    ====
    Gestire la history completa su un file a parte
    e l'undo della cancellazione
    """
    
    def __init__(self):
        self.dir = None
        self.autostore = None
    
    def __missing__ (self, user):
        self.load(user)
        return self[user]
    
    def open(self, data_dir='data', autostore=0):
        self.dir = data_dir
        check_dir(self.dir)
        if autostore:
            self.autostore = AutoStore(self.store, (), delay=autostore)
            self.autostore.start()
        else:
            self.autostore = None
    
    def close(self):
        if self.autostore:
            self.autostore.join()
        self.store()
    
    def load(self, user):
        fn = os.path.join(self.dir, user + '.json')
        if os.path.isfile(fn):
            buf = open(fn, 'rb').read()
            self[user] = json.loads(buf)
        else:
            self[user] = {}
            open(fn, 'wb').write(json.dumps(self[user]))
    
    def store(self, user=''):
        if user:
            self.store_user(user)
        else:
            for user in self.keys():
                self.store_user(user)
    
    def store_user(self, user):
        md5 = os.path.join(self.dir, user + '.md5')
        fn = os.path.join(self.dir, user + '.json')
        
        buf = json.dumps(self[user])
        new_md5 = hashlib.md5(buf).digest()
        if os.path.isfile(md5):
            old_md5 = open(md5).read()
        else:
            old_md5 = ''
        if new_md5 != old_md5:
            open(fn, 'wb').write(buf)
            open(md5, 'wb').write(new_md5)
            logging.info('%s saved' % fn)
        else:
            logging.info('%s: nothing to save' % user)
    
    def get_note(self, user, key):
        return self[user].get(key, {})
    
    def set_note(self, user, key, content=''):
        note = self.get_note(user, key)
        now = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        if not note:
            note['ins_ts'] = now
            note['previus_content'] = ''
        else:
            note['previus_content'] = note['content']
        note['upd_ts'] = now
        note['user'] = user
        note['key'] = key
        note.setdefault('version', 0)
        note['version'] += 1
        note['content'] = content
        self[user][key] = note
        return self[user][key]
    
    def undo_last_change(self, user, key):
        note = self.get_note(user, key)
        self.set_note(user, key, note['previus_content'])
    
    def delete(self, user, key):
        note = self.get_note(user, key)
        if note:
            del self[user][key]
    
    def change_key(self, user, key, new_key):
        note = self.get_note(user, key)
        self.delete(user,key)
        note['key'] = new_key
        self[user][key] = note
    
    def get_list(self, user, order_by='key'):
        result = self[user].values()
        if order_by == 'key':
            sort_key = lambda (key, x): key.lower()
        else:
            sort_key = None
        result = sort_by(result, order_by, sort_key)
        if order_by == 'upd_ts':
        	result.reverse()
        return result
    
    def add_tag(self, user, key, tag):
        pass
    
    def remove_tag(self, user, key, tag):
        pass
    
    def search(self, user, pattern, order_by='key'):
        ll = self.get_list(user, order_by)
        result = []
        pat_l = pattern.lower()
        for rec in ll:
            if rec['key'].lower().find(pat_l) > -1:
                result.append(rec)
            elif rec['content'].lower().find(pat_l) > -1:
                result.append(rec)
        return result
    
    def get_by_tags(self, user, tags=[]):
        pass

NDB = Notes()

def test():
    import getpass
    import pprint
    user = getpass.getuser()
    n = Notes()
    n.set_note(user, 'pippo', 'Ciao Mondo')
    n.set_note(user, 'pluto', 'Mangiamoci una pizza!')
    n.set_note('paperino','paperino', 'questa nota e\' solo mia!')
    
    print
    print 'get_list'
    pprint.pprint(n.get_list(user))
    
    print
    print 'get_list user paperino'
    pprint.pprint(n.get_list('paperino'))
    
    print 
    print "n.get_note(user, 'pippo')"
    pprint.pprint(n.get_note(user, 'pippo'))
    
    print
    print "n.get_note(user, 'paperino')"
    pprint.pprint(n.get_note(user, 'paperino'))
    
    print
    n.delete(user, 'pluto')
    print "n.get_list(user) dopo cancellazione pluto"
    pprint.pprint(n.get_list(user))
    n.set_note(user, 'pluto', 'Mangiamoci una pizza!')
    n.set_note(user, 'alpha', 'kfg erj forojweor\njgweorjgw\neor j werj weor!')
    
    n.store()

if __name__ == '__main__':
    test()